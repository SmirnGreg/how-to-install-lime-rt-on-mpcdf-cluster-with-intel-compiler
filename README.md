# How to install lime-rt on MPCDF cluster with intel compiler

Instruction based on the attempt made by @smirngreg, Sierk van Terwisga, and Riccardo Francheschi 15.05.2020

https://us04web.zoom.us/j/78409941974?pwd=WFFKV2ZPSEdnMzR1b3UvYUVMUXpJQT09 ← here! Zoom link


# Access to COBRA:

https://www.mpcdf.mpg.de/services/computing/cobra/access-to-the-HPC-system

There is a possibility to reduce the amount of typing by adding the followint 
to the  `~/.ssh/config` file:
 
```
Host gatezero                                                                                                                                                                                                                                                              
 HostName gatezero.mpcdf.mpg.de                                                                                                                                                                                                                                           
 User  **username**                                                                                                                                                                                                                                                           
 GSSAPIAuthentication yes                                                                                                                                                                                                                                                 
 GSSAPIDelegateCredentials yes                                                                                                                                                                                                                                            
                                                                                                                                                                                                                                                                          
Host cobra                                                                                                                                                                                                                                                                 
 HostName cobra.mpcdf.mpg.de                                                                                                                                                                                                                                              
 User **username**                                                                                                                                                                                                                                                           
 GSSAPIAuthentication yes                                                                                                                                                                                                                                                 
 GSSAPIDelegateCredentials yes                                                                                                                                                                                                                                            
 ProxyCommand ssh -q -W %h:%p gatezero
```  

Then `ssh cobra` works asking for the password twice (!).

Also a solution from Sierk:

```bash
ssh -J username@gatezero.mpcdf.mpg.de username@cobra.mpcdf.mpg.de
```

# Download LIME and QHULL as of 15.05.2020 latest release versions:

```bash
wget https://github.com/lime-rt/lime/archive/v1.9.5.tar.gz                                                                                                 
tar xzvf v1.9.5.tar.gz                                                                                                                                     

wget http://www.qhull.org/download/qhull-2019-src-7.3.2.tgz                                                                                                
tar xzvf qhull-2019-src-7.3.2.tgz

wget http://heasarc.gsfc.nasa.gov/FTP/software/fitsio/c/cfitsio-3.48.tar.gz
tar xzvf cfitsio*.tar.gz
```

### Configure LIME
```
cd lime-*
export LIME_PATH=$PWD
./configure
```


### Check dependencies (what if they are installed already?)lgsl_rng_ranlxs2

```
cd example/
$LIME_PATH/lime model.c
```

Lime crashes with no gsl.

## Install dependensies

### Load gsl
```
module load gsl
```

In lime Makefile: replace gcc with icc
```makefile
CPPFLAGS += 
            -I/${GSL_HOME}/include \
```

### qhull

Now it is passing the gsl and crashes on qhull

```bash
cd qhull-*/build                                 
cmake -G "Unix Makefiles" \
  -DCMAKE_INSTALL_PREFIX=$LIME_PATH \
  -DCMAKE_C_COMPILER=icc -DCMAKE_CXX_COMPILER=icpc \
  ..
make
make install
```

Go back to the lime example -- it fails now on `qhull_a.h`

Tell `lime.h` to search for `libqhull/qhull_a.h` instead of `qhull_a.h`. 
It should be `<libqhull/qhull_a.h>`

https://github.com/lime-rt/lime/issues/#issuecomment-518642881


### cfitsio

Now we go to cfitsio:
```bash
cd cfitsio*
mkdir build && cd build
cmake -G "Unix Makefiles" \
  -DCMAKE_INSTALL_PREFIX=$LIME_PATH \
  -DCMAKE_C_COMPILER=icc -DCMAKE_CXX_COMPILER=icpc \
  ..
make
make install
```

### gsl again!

Go back to the example… Now it crashes on cannot find `lgsl` `lgslcblas`

Do `gsl-config --libs` 
In my case, `-L/mpcdf/soft/SLE_12/packages/skylake/gsl/intel_19.0.4-19.0.4/2.4/lib -lgsl -lgslcblas -lm`.

Copy the output to the Makefile:
```makefile
LIBS +=  <HERE>
```
Also replace
```makefile
LDFLAGS += -l${LIB_QHULL} -lcfitsio -lncurses \            
  <HERE> \  # what to link
  -Wl,-rpath,<only path HERE>  # where to search for it afterward
```

## keylimepie

There is a `keylimepie` package by Teague, Smirnov-Pinchukov,
to run multiple instances of Lime on slurm clusters.
https://gitlab.com/SmirnGreg/keylimepie
